package workable.com.testsharedelements;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MovieFragment extends Fragment {

    public static MovieFragment newInstance(Context context){
        MovieFragment fragment = new MovieFragment();


        return fragment;
    }

    public MovieFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView =  inflater.inflate(R.layout.fragment_movie, container, false);

        GridView gridview = (GridView)rootView.findViewById(R.id.gridview);
        Resources res = getActivity().getResources();

        final ArrayList<Movie> movies = new ArrayList<Movie>();
        movies.add(new Movie(res.getDrawable(R.drawable.poster_about_time), "About Time"));
        movies.add(new Movie(res.getDrawable(R.drawable.poster_enders_game), "Enders Game"));
        movies.add(new Movie(res.getDrawable(R.drawable.poster_enough_said), "Enough Said"));
        movies.add(new Movie(res.getDrawable(R.drawable.poster_the_fifth_estate), "The fifth estate"));
        movies.add(new Movie(res.getDrawable(R.drawable.poster_gravity), "Gravity"));
        movies.add(new Movie(res.getDrawable(R.drawable.poster_cloudy_with_a_chance_of_meatballs_2), "Cloudy with a chance of meatballs"));
        movies.add(new Movie(res.getDrawable(R.drawable.poster_prisoners), "Prisoners"));
        movies.add(new Movie(res.getDrawable(R.drawable.poster_runner_runner), "Runner Runner"));
        movies.add(new Movie(res.getDrawable(R.drawable.poster_the_counselor), "The counselor"));
        movies.add(new Movie(res.getDrawable(R.drawable.poster_thor), "Thor"));

        MovieAdapter adapter = new MovieAdapter(getActivity(), movies);
        gridview.setAdapter(adapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                setExitTransition(TransitionInflater.from(getActivity()).inflateTransition(android.R.transition.explode));
                setSharedElementReturnTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.trans_move));

                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, MovieDetailFragment.newInstance(getActivity(), movies.get(i)))
                        .addToBackStack(null)
                        .commit();
            }
        });

        return rootView;
    }
}
