package workable.com.testsharedelements;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MovieDetailFragment extends Fragment {



    public static MovieDetailFragment newInstance(Context context, Movie movie){
        MovieDetailFragment fragment = new MovieDetailFragment();
        fragment.movie = movie;

        TransitionInflater inflater = TransitionInflater.from(context);
        fragment.setSharedElementEnterTransition(inflater.inflateTransition(R.transition.trans_move));
        fragment.setEnterTransition(inflater.inflateTransition(android.R.transition.slide_bottom));

        return fragment;
    }

    private Movie movie;


    public MovieDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        ImageView poster = (ImageView) rootView.findViewById(R.id.movie_detail_poster);
        poster.setImageDrawable(movie.getDrawable());

        final BottomSheetLayout bottomSheetLayout = (BottomSheetLayout)rootView.findViewById(R.id.bottomsheet);

        rootView.findViewById(R.id.show_bottomSheet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuSheetView menuSheetView =
                        new MenuSheetView(getActivity(), MenuSheetView.MenuType.LIST, "Create...", new MenuSheetView.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                Toast.makeText(getActivity(), item.getTitle(), Toast.LENGTH_SHORT).show();
                                if (bottomSheetLayout.isSheetShowing()) {
                                    bottomSheetLayout.dismissSheet();
                                }
                                return true;
                            }
                        });
                menuSheetView.inflateMenu(R.menu.menu_main);
                bottomSheetLayout.showWithSheetView(menuSheetView);
            }
        });



        return rootView;
    }
}
